use mongodb::Client;

#[derive(Clone)]
pub struct MongoDbClient {
    pub client: Client
}