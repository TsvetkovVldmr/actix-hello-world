use mongodb::Client;
use crate::data::MongoDbClient;
use crate::models::User;

const DB_NAME: &str = "actixdb";
const COLLECTION_NAME: &str = "users";

impl MongoDbClient {
    pub async fn new(mongodb_uri: String) -> Self {
        let client = Client::with_uri_str(mongodb_uri)
            .await
            .expect("Failed");

        MongoDbClient {client}
    }

    pub async fn create(&self, user: User) {

    }
}