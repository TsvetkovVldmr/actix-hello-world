use actix_web::{get, web, Scope, Responder, HttpResponse, post};



pub fn user_scope() -> Scope {
    web::scope("/user")
        .service(get_users)
        .service(create_user)
}

#[get("/")]
async fn get_users() -> impl Responder {
    HttpResponse::Ok()
}

#[post("/")]
async fn create_user(req: String) -> impl Responder {
    HttpResponse::Ok()
}