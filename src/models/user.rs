use uuid::Uuid;
use serde::{Deserialize, Serialize};


#[derive(Deserialize, Serialize)]
pub struct User {
    pub id: Uuid,
    pub name: String,
    pub login: String,
    pub password: String
}